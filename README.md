Excel Like spreadsheet using Javascript
--------------------------------------------------------------------------

A simple excel like spreadsheet using plain javascript

#Features

1. Entirely client side, so blazingly fast
2. Support for excel like expressions in the cells, Eg. =A1+B2*E5
3. Add rows and columns dynamically
4. Sort in ascending and descending order based on columns
5. Store in localStorage, or download data as JSON
6. Load sheets from localStorage or upload JSON to load the spreadsheet.

#Usage

* Create a sheet using Blank for the first time
* Add rows/columns dynamically
* Use formulas/expressions in any field (Eg. =A1+5*B3)
* If you click on a column header that is already sorted, sorting order is reversed (ascending->descending and vice-versa).
* Save to localStorage/ Download the sheet as JSON (To store in localStorage, name of the sheet has to be given)
* Load from localStorage or upload previously downloaded JSON file to load the sheet.