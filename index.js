var tmpStorage = {};
var nRows = localStorage['nRows'];
var nColumns = localStorage['nColumns'];
var DATA = {};
var sortState = {};

function init() 
{
    // Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        //All the File APIs are supported. 
    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
    document.getElementById('dataFile').addEventListener('change', loadFromFile, false);
}

function loadFromFile(event) 
{
    if (!event)
        event = window.event;
    var target = event.target || event.srcElement;
    var files = target.files;
    // Read only the first file
    var file = files[0];
    /*        if (!file.type.match('data/json'))
                return;*/
    var reader = new FileReader();
    reader.onload = (function(theFile) {
        return function(e) {
            if (!e)
                e = window.event;
            var target = e.target || e.srcElement;
            try {
                var json = target.result;
                tmpStorage = JSON.parse(json);
                create(1);
                document.getElementById('fileName').value = theFile.name.split('.json')[0];
            } catch (e) {
                if (e === null || e.message === null) {
                    e = {};
                    e.message = 'Unknown error';
                }
                alert("Invalid/Corrupt File: " + e.message + "\nCreating Blank SpreadSheet");
                create(2);
            }
        };
    })(file);

    // Read the file
    reader.readAsText(file);
}

/*
 * loadFromLocalStorage : true  -> load data from localstorage
 *                        false -> Data was already loaded from input file,
 *                                 so just use it.  
 */
function load(loadFromLocalStorage) 
{
    if (loadFromLocalStorage) {
        var name = document.getElementById('loadFile').value;
        tmpStorage = JSON.parse(localStorage[name]);
    }
    nRows = tmpStorage['nRows'];
    nColumns = tmpStorage['nColumns'];
}

function downloadFile() 
{
    var name = document.getElementById('fileName').value;
    var data = JSON.stringify(tmpStorage);
    var downloadUrl = 'data:' + data;
    if (name === null || typeof name === 'undefined' || name === '\'\'') {
        name = 'BLANK';
    }
    var fileName = name + '.json';
    var blob = new Blob([data], {
        type: 'text/json'
    })
    if (typeof window.navigator.msSaveBlob !== 'undefined') {
        // IE workaround 
        window.navigator.msSaveBlob(blob, fileName);
    } else {
        var URL = window.URL || window.webkitURL;
        var downloadUrl = URL.createObjectURL(blob);
        // use HTML5 a[download] attribute to specify
        // fileName
        var a = document.createElement("a");
        // safari doesn't support this yet
        if (typeof a.download === 'undefined') {
            window.location = downloadUrl;
        } else {
            a.href = downloadUrl;
            a.download = fileName;
            document.body.appendChild(a);
            a.click();
        }
    }
}

/*  
 * loadFrom : 0 -> load from localstorage
 *            1 -> load from file
 *            2 -> Create a blank sheet
 */
function create(loadFrom) 
{
    if (loadFrom === null) {
        loadFrom = 2;
    }
    loadFrom = parseInt(loadFrom);
    if (loadFrom === 0) {
        load(true);
        buildSpreadSheet(true);
    } else if (loadFrom === 1) {
        load(false);
        buildSpreadSheet(true);
    } else if (loadFrom === 2)
        buildSpreadSheet(false);
    document.getElementById('spreadsheet').style.display = "block";
    document.getElementById('createForm').style.display = "none";
}
/*
 * Sort based on a column
 */
function sortColumn(event) 
{
    if (!event)
        event = window.event;
    var target = event.target || event.srcElement;
    var targetId = target.id;
    if (targetId === '') {
        targetId = target.getElementsByTagName('span')[0].id;
    }
    targetId = parseInt(targetId);
    if (sortState !== null && sortState.column >= 0) {
        var ele = document.getElementById('arrow' + sortState.column);
        if (ele !== null) {
            var parent = ele.parentNode;
            parent.removeChild(ele);
        }
    }
    var nc = targetId - 'A'.charCodeAt(0);
    var order = 1;
    if (sortState && sortState.column === nc) {
        if (sortState.order !== null && typeof sortState.order !== 'undefined')
            order = -1 * parseInt(sortState.order);
    }
    var vals = new Array(nRows - 1);
    for (var k = 0; k < nRows - 1; k++) {
        vals[k] = new Array(nColumns - 1);
    }
    for (var i = 1; i < nRows; i++) {
        for (var j = 1; j < nColumns; j++) {
            var id = String.fromCharCode('A'.charCodeAt(0) - 1 + j) + i;
            var elem = document.getElementById(id);
            vals[i - 1][j - 1] = elem.value || '';
        }
    }
    vals.sort(function(a, b) {
        //console.log(nc);
        if (typeof a[nc] === 'undefined')
            return 1 * order;
        if (typeof b[nc] === 'undefined')
            return -1 * order;
        if (a[nc] === '' && b[nc] !== '') {
            return 1 * order;
        }
        if (a[nc] !== '' && b[nc] === '') {
            return -1 * order;
        }
        return ((a[nc] == b[nc]) ? 0 : ((a[nc] > b[nc]) ? 1 : -1)) * order;
    });
    for (var i = 1; i < nRows; i++) {
        var s = '';
        for (var j = 1; j < nColumns; j++) {
            s += '    ' + vals[i - 1][j - 1];
            var id = String.fromCharCode('A'.charCodeAt(0) - 1 + j) + i;
            document.getElementById(id).value = vals[i - 1][j - 1];
            tmpStorage[id] = vals[i - 1][j - 1]
            //console.log(vals[i - 1][j - 1] + ',' + i + ',' + j, ',' + id);
        }
    }
    sortState['column'] = nc;
    // 1 -> Ascending
    // -1 -> Descending
    sortState['order'] = order;

    var arrow;
    if (order === 1) {
        arrow = '&#x25B2;';
    } else {
        arrow = '&#x25BC;';
    }
    var arrowSpan = document.createElement('span');
    arrowSpan.id = 'arrow' + nc;
    arrowSpan.innerHTML = arrow;
    target.appendChild(arrowSpan);
    //console.log(vals);
}

function save(download) 
{
    if (!download) {
        download = false;
    }
    var name = document.getElementById('fileName').value;
    localStorage[name] = {};
    tmpStorage['nRows'] = nRows;
    tmpStorage['nColumns'] = nColumns;
    localStorage[name] = JSON.stringify(tmpStorage);
    if (download)
        downloadFile();
}

function buildSpreadSheet(loadFromPrev) 
{
    if (nRows === undefined || nRows === null) {
        nRows = 6;
    }
    if (nColumns === undefined || nRows === null) {
        nColumns = 6;
    }
    if (!loadFromPrev) {
        nRows = nColumns = 6;
    }
    for (var i = 0; i < nRows; i++) {
        var row = document.querySelector("table").insertRow(-1);
        for (var j = 0; j < nColumns; j++) {
            var letter = String.fromCharCode("A".charCodeAt(0) + j - 1);
            if (i == 0 && j > 0) {
                var elem = document.createElement("span");
                elem.id = "A".charCodeAt(0) + j - 1;
                elem.className = 'th';
                elem.appendChild(document.createTextNode(letter));
                var newCell = row.insertCell(-1);
                newCell.setAttribute('onclick', 'sortColumn(event)');
                newCell.appendChild(elem);
            } else {
                row.insertCell(-1).innerHTML = i && j ? "<input class='cell' id='" + letter + i + "'/>" : i || letter;
            }
        }
    }
    var INPUTS = [].slice.call(document.querySelectorAll("input"));
    INPUTS.forEach(function(elm) {
        doFormatting(elm);
    });
    (window.computeAll = function() {
        INPUTS.forEach(function(elm) {
            try {
                elm.value = DATA[elm.id];
            } catch (e) {}
        });
    })();
}

function doFormatting(elm) 
{
    elm.onfocus = function(e) {
        e.target.value = tmpStorage[e.target.id] || "";
    };
    elm.onblur = function(e) {
        tmpStorage[e.target.id] = e.target.value;
        computeAll();
    };
    var getter = function() {
        var value = tmpStorage[elm.id] || "";
        if (value.charAt(0) == "=") {
            with(DATA) return eval(value.substring(1));
        } else {
            return isNaN(parseFloat(value)) ? value : parseFloat(value);
        }
    };
    Object.defineProperty(DATA, elm.id, {
        get: getter
    });
    Object.defineProperty(DATA, elm.id.toLowerCase(), {
        get: getter
    });
}

function createInputElement(id) 
{
    var element = document.createElement("input");
    element.id = id;
    element.className = 'cell';
    return element;
}

function addRow() 
{
    var row = document.querySelector("table").insertRow(-1);
    for (var j = 0; j < nColumns; j++) {
        var letter = String.fromCharCode("A".charCodeAt(0) + j - 1);
        var cell = row.insertCell(-1);
        if (j == 0) {
            cell.innerHTML = nRows;
        } else {
            var ipEle = createInputElement((letter + nRows).toString());
            cell.appendChild(ipEle);
            doFormatting(ipEle);
            ipEle.value = DATA[ipEle.id];
        }
    }
    nRows += 1;
    localStorage["nRows"] = nRows;
}

function addColumn() 
{
    var rows = document.querySelector("table").rows;
    for (var i = 0; i < nRows; i++) {
        var letter = String.fromCharCode("A".charCodeAt(0) + nColumns - 1);
        var cell = rows[i].insertCell(-1);
        /*                if (i == 0)
                        { 
                            cell.innerHTML = letter;
                        }*/
        if (i == 0) {
            var elem = document.createElement("span");
            elem.appendChild(document.createTextNode(letter));
            elem.id = "A".charCodeAt(0) + i - 1;
            elem.className = 'th';
            cell.setAttribute('onclick', 'sortColumn(event)');
            cell.appendChild(elem);
        } else {
            var ipEle = createInputElement((letter + i).toString());
            cell.appendChild(ipEle);
            doFormatting(ipEle);
            ipEle.value = DATA[ipEle.id];
        }
    }
    nColumns += 1;
    localStorage["nColumns"] = nColumns;
}
